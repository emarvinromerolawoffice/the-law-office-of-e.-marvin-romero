The Law Office of E. Marvin Romero was established to give any person a voice in effectuating change and holding others who do harm accountable. The firm handles a variety of personal injury claim ranging from automobile accidents to wrongful death.

Address: 10400 Academy Rd NE, #345, Albuquerque, NM 87111, USA

Phone: 505-595-2100

Website: https://emrpilaw.com
